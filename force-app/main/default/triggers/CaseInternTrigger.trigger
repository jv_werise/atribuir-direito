trigger CaseInternTrigger on Case (before insert, before update, before delete, after insert, after update, after undelete) {
    
    if(Trigger.isInsert){
        if(Trigger.isAfter){
            //lists to handle the logic
            list<Case> melhoriaList = new list<case>();
            list<Case> correcaoList = new list<case>();

            for(Case cs : Trigger.new){
                if(cs.type == 'Melhoria'){
                    melhoriaList.add(cs);
                }
                if(cs.type == 'Correção'){
                    correcaoList.add(cs);
                }
            }
        }
    }
}